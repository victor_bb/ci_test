<?php

/**
 * Created by PhpStorm.
 * User: mr.incognito
 * Date: 27.01.2020
 * Time: 10:10
 */
class Comment_model extends CI_Emerald_Model
{
    use Likable;

    const CLASS_TABLE = 'comment';

    const FULL_INFO = 1;
    const RESOURCE_INFO = 2;

    public $create_rules = [
        [
            'field' => 'post_id',
            'rules' => 'is_natural_no_zero|required'
        ],
        [
            'field' => 'parent_id',
            'rules' => 'is_natural|required'
        ],
        [
            'field' => 'body',
            'rules' => 'trim|required|max_length[3000]'
        ]
    ];

    /** @var int */
    protected $parent_id;
    /** @var int */
    protected $children_count;
    /** @var int */
    protected $user_id;
    /** @var int */
    protected $assign_id;
    /** @var string */
    protected $text;

    /** @var string */
    protected $time_created;
    /** @var string */
    protected $time_updated;

    // generated
    protected $children;
    protected $user;

    /**
     * @return int
     */
    public function getParentId(): int
    {
        return $this->parent_id;
    }

    /**
     * @param int $parent_id
     */
    public function setParentId(int $parent_id): void
    {
        $this->parent_id = $parent_id;
    }

    /**
     * @return mixed
     */
    public function getChildren()
    {
        return $this->children;
    }

    /**
     * @param mixed $children
     */
    public function setChildren($children): void
    {
        $this->children = $children;
    }

    public function increaseCommentCount()
    {
        $this->save('children_count', $this->getChildrenCount() + 1);
    }

    /**
     * @return int
     */
    public function getChildrenCount(): int
    {
        return $this->children_count;
    }

    /**
     * @param int $children_count
     */
    public function setChildrenCount(int $children_count): void
    {
        $this->children_count = $children_count;
    }


    /**
     * @return int
     */
    public function get_user_id(): int
    {
        return $this->user_id;
    }

    /**
     * @param int $user_id
     *
     * @return bool
     */
    public function set_user_id(int $user_id)
    {
        $this->user_id = $user_id;
        return $this->save('user_id', $user_id);
    }

    /**
     * @return int
     */
    public function get_assign_id(): int
    {
        return $this->assign_id;
    }

    /**
     * @param int $assign_id
     *
     * @return bool
     */
    public function set_assign_id(int $assign_id)
    {
        $this->assign_id = $assign_id;
        return $this->save('assing_id', $assign_id);
    }


    /**
     * @return string
     */
    public function get_text(): string
    {
        return $this->text;
    }

    /**
     * @param string $text
     *
     * @return bool
     */
    public function set_text(string $text)
    {
        $this->text = $text;
        return $this->save('text', $text);
    }


    /**
     * @return string
     */
    public function get_time_created(): string
    {
        return $this->time_created;
    }

    /**
     * @param string $time_created
     *
     * @return bool
     */
    public function set_time_created(string $time_created)
    {
        $this->time_created = $time_created;
        return $this->save('time_created', $time_created);
    }

    /**
     * @return string
     */
    public function get_time_updated(): string
    {
        return $this->time_updated;
    }

    /**
     * @param string $time_updated
     *
     * @return bool
     */
    public function set_time_updated(int $time_updated)
    {
        $this->time_updated = $time_updated;
        return $this->save('time_updated', $time_updated);
    }

    // generated
    /**
     * @return mixed
     */
    public function get_comments()
    {
        return $this->comments;
    }

    /**
     * @return User_model
     */
    public function get_user():User_model
    {
        if (empty($this->user))
        {
            try {
                $this->user = new User_model($this->get_user_id());
            } catch (Exception $exception)
            {
                $this->user = new User_model();
            }
        }
        return $this->user;
    }

    function __construct($id = NULL)
    {
        parent::__construct();

        App::get_ci()->load->model('User_model');


        $this->set_id($id);
    }

    public function reload(bool $for_update = FALSE)
    {
        parent::reload($for_update);

        return $this;
    }

    public static function create(array $data)
    {
        App::get_ci()->s->from(self::CLASS_TABLE)->insert($data)->execute();
        return new static(App::get_ci()->s->get_insert_id());
    }

    public function delete()
    {
        $this->is_loaded(TRUE);
        App::get_ci()->s->from(self::CLASS_TABLE)->where(['id' => $this->get_id()])->delete()->execute();
        return (App::get_ci()->s->get_affected_rows() > 0);
    }

    /**
     * @param int $post_id
     * @return self[]
     * @throws Exception
     */
    public static function get_all_by_assign_id(int $post_id)
    {
        $ex = new self();

        $comments = App::get_ci()->s->from(self::CLASS_TABLE)->where(['assign_id' => $post_id, 'parent_id' => 0])->orderBy('time_created','ASC')->many();

        $classarray = array_map([$ex, 'fff'], $comments);

        return $classarray;
    }

    protected function fff($commentItem)
    {
        $outPut = [];

        //foreach ($comments as $index => $item) {
            $commentModel = (new self())->set($commentItem);

            if ($commentModel->getChildrenCount() > 0) {
                $commentModel->setChildren(
                    array_map(
                        [$this, 'fff'],
                        App::get_ci()->s->from(self::CLASS_TABLE)->where(['assign_id' => $commentModel->get_assign_id(), 'parent_id' => $commentModel->get_id()])->orderBy('time_created','ASC')->many()
                    )
                );
            }

            //$outPut[] = $commentModel;
        //}

        return $commentModel;
    }

    /**
     * @param $data
     * @param int $preparation
     *
     * @return stdClass[]
     *
     * @throws Exception
     */
    public static function preparation($data, $preparation = 0)
    {
        switch ($preparation)
        {
            case self::FULL_INFO:
                return self::_preparation_full_info($data);
            case self::RESOURCE_INFO:
                return self::_preparation_resource_info($data);
            default:
                throw new Exception('undefined preparation type');
        }
    }


    /**
     * @param self[] $data
     * @return stdClass[]
     */
    private static function _preparation_full_info($data)
    {
        $ret = [];

        foreach ($data as $d){
            $o = new stdClass();

            $o->id = $d->get_id();
            $o->text = $d->get_text();

            $o->user = User_model::preparation($d->get_user(),'main_page');

            $o->likes = $d->get_likes();

            if ($d->getChildrenCount() > 0) {
//                foreach ($d->getChildren() as $child) {
                    $o->children = self::_preparation_full_info($d->getChildren());
//                }
            } else {
                $o->children = [];
            }

            $o->time_created = $d->get_time_created();
            $o->time_updated = $d->get_time_updated();

            $ret[] = $o;
        }


        return $ret;
    }

    /**
     * @param Comment_model[] $data
     *
     * @return array
     *
     * @throws Exception
     */
    protected static function _preparation_resource_info(array $data)
    {
        $output = [];

        foreach ($data as $entry) {
            $output[] = [
                'id' => $entry->get_id(),
                'like' => $entry->get_likes(),
                'text' => $entry->get_text(),
                'user' => User_model::preparation($entry->get_user(),'main_page'),
                'children' => [],
                'created' => $entry->get_time_created(),
                'updated' => $entry->get_time_updated(),
            ];
        }

        return $output;
    }
}
