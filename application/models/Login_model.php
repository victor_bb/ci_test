<?php
class Login_model extends CI_Model {

    public $login_rules = [
        [
            'field' => 'login',
            'label' => 'Логин или E-mail',
            'rules' => 'trim|required|max_length[50]'
        ],
        [
            'field' => 'password',
            'label' => 'Пароль',
            'rules' => 'trim|required|max_length[30]'
        ]
    ];

    public static function logout()
    {
        App::get_ci()->session->unset_userdata('id');
    }

    public static function start_session(int $user_id)
    {
        // если перенедан пользователь
        if (empty($user_id))
        {
            throw new CriticalException('No id provided!');
        }

        App::get_ci()->session->set_userdata('id', $user_id);
    }
}
