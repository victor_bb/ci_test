<?php

/**
 * Class Likes_model
 */
class Likes_model extends CI_Model
{
    const LIKES_TYPE_POST = 'post';
    const LIKES_TYPE_COMMENT = 'comment';

    public $add_like_rules = [
        [
            'field' => 'entry_id',
            'rules' => 'is_natural|required'
        ],
        [
            'field' => 'entry_type',
            'rules' => 'required|in_list[comment,post]'
        ]
    ];

    /**
     * @param int $userId
     * @param int $id
     * @param string $type
     *
     * @return bool
     *
     * @throws Exception
     */
    public function likeEntry(int $userId, int $id, string $type): bool
    {
        if (!$this->entryTypeLikable($type)) {
            return false;
        }

        $s = App::get_ci()->s->start_trans();

        // decrease user likes amount
        $userDecreaseResult =
            $s
                ->sql([
                    'UPDATE',
                    User_model::CLASS_TABLE,
                    'SET',
                    'likes_amount = likes_amount - 1',
                    'WHERE id = ' . $userId
                ])
                ->execute()
        ;

        $likeResult = false;

        // like entry
        switch ($type) {
            case self::LIKES_TYPE_POST:
                $likeResult =
                    $s
                        ->sql([
                            'UPDATE',
                            Post_model::CLASS_TABLE,
                            'SET',
                            'likes = likes + 1',
                            'WHERE id = ' . $id
                        ])
                        ->execute()
                ;

                break;

            case self::LIKES_TYPE_COMMENT:
                $likeResult =
                    $s
                        ->sql([
                            'UPDATE',
                            Comment_model::CLASS_TABLE,
                            'SET',
                            'likes = likes + 1',
                            'WHERE id = ' . $id
                        ])
                        ->execute()
                ;

                break;
        }

        if ($userDecreaseResult && $likeResult) {
            $s->commit();

            return true;
        }

        $s->rollback();

        return false;
    }

    public function getEntryLikesAmount(int $id, string $type): int
    {
        switch ($type) {
            case self::LIKES_TYPE_POST:
                $post = new Post_model($id);

                return $post->get_likes();

            case self::LIKES_TYPE_COMMENT:
                $comment = new Comment_model($id);

                return $comment->get_likes();

            default:
                return 0;
        }
    }

    /**
     * @param string $type
     *
     * @return bool
     */
    protected function entryTypeLikable(string $type): bool
    {
        switch ($type) {
            case self::LIKES_TYPE_POST:
                return true;

            case self::LIKES_TYPE_COMMENT:
                return true;

            default:
                return false;
        }
    }
}
