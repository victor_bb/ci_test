<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class MY_Controller extends CI_Controller
{
    const HTTP_OK = 200;
    const HTTP_BAD = 400;
    const HTTP_UNAUTHORIZED = 401;
    const HTTP_FORBIDDEN = 403;
    const HTTP_SERVER_ERROR = 500;

    public function __construct()
    {
        parent::__construct();
    }

    public function __destruct()
    {

    }

    protected function checkAuth()
    {
        if (!User_model::is_logged()){
            return $this->response_error(CI_Core::RESPONSE_GENERIC_NEED_AUTH);
        }
    }

    /**
     * @return mixed
     */
    protected function getJSONContentFromInputStream()
    {
        return $this->security->xss_clean($this->input->raw_input_stream);
    }

    /**
     * @return array
     */
    protected function jsonRequest()
    {
        // забираем json из входящего потока и декодируем в ассоциотивный массив
        $result = json_decode($this->getJSONContentFromInputStream(), true);

        if (JSON_ERROR_NONE === json_last_error()) {
            return $result;
        }

        return null;
    }
}
