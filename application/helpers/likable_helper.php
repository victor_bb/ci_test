<?php

/**
 * Trait Likable
 */
trait Likable
{
    protected $likes;

    /**
     * @return int
     */
    public function get_likes(): int
    {
        return $this->likes ?? 0;
    }

    /**
     * @param int $likes
     */
    public function set_likes(int $likes): void
    {
        $this->likes = $likes;
    }
}
