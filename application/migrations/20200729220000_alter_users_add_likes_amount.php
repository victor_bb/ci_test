<?php
/**
 * Created by PhpStorm.
 * User: victor
 * Date: 25.07.20
 * Time: 21:48
 */

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Class Migration_Alter_users_add_likes_amount
 *
 * @property CI_DB_forge $dbforge
 */
class Migration_Alter_users_add_likes_amount extends CI_Migration {

    public function up()
    {
        $this->dbforge->add_column(
            'user',
            [
                'likes_amount' => [
                    'type' => 'INT',
                    'unsigned' => true,
                    'NULL' => false,
                    'default' => 0,
                ],
            ]
        );

        $this->dbforge->add_column(
            'post',
            [
                'likes' => [
                    'type' => 'INT',
                    'unsigned' => true,
                    'NULL' => false,
                    'default' => 0,
                ],
            ]
        );

        $this->dbforge->add_column(
            'comment',
            [
                'likes' => [
                    'type' => 'INT',
                    'unsigned' => true,
                    'NULL' => false,
                    'default' => 0,
                ],
            ]
        );
    }

    public function down()
    {
        $this->dbforge->drop_column('user', 'likes_amount');
        $this->dbforge->drop_column('post', 'likes');
        $this->dbforge->drop_column('comment', 'likes');
    }
}
