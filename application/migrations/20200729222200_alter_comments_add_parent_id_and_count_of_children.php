<?php
/**
 * Created by PhpStorm.
 * User: victor
 * Date: 25.07.20
 * Time: 21:48
 */

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Class Migration_Alter_users_add_likes_amount
 *
 * @property CI_DB_forge $dbforge
 */
class Migration_alter_comments_add_parent_id_and_count_of_children extends CI_Migration {

    public function up()
    {
        $this->dbforge->add_column(
            'comment',
            [
                'parent_id' => [
                    'type' => 'INT',
                    'unsigned' => true,
                    'NULL' => false,
                    'default' => 0,
                ],
                'children_count' => [
                    'type' => 'INT',
                    'unsigned' => true,
                    'NULL' => false,
                    'default' => 0,
                ],
            ]
        );

    }

    public function down()
    {
        $this->dbforge->drop_column('comment', 'parent_id');
        $this->dbforge->drop_column('comment', 'children_count');
    }
}
