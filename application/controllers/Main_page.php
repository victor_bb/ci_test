<?php

/**
 * Created by PhpStorm.
 * User: mr.incognito
 * Date: 10.11.2018
 * Time: 21:36
 */

/**
 * Class Main_page
 *
 * @property Login_model $Login_model
 * @property User_model $User_model
 * @property CI_Form_validation $form_validation
 */
class Main_page extends MY_Controller
{

    public function __construct()
    {
        parent::__construct();

        App::get_ci()->load->model('User_model');
        App::get_ci()->load->model('Login_model');
        App::get_ci()->load->model('Post_model');

        if (is_prod())
        {
            die('In production it will be hard to debug! Run as development environment!');
        }
    }

    public function index()
    {
        $user = User_model::get_user();



        App::get_ci()->load->view('main_page', ['user' => User_model::preparation($user, 'default')]);
    }

    public function get_all_posts()
    {
        $posts =  Post_model::preparation(Post_model::get_all(), 'main_page');
        return $this->response_success(['posts' => $posts]);
    }

    public function get_post($post_id){ // or can be $this->input->post('news_id') , but better for GET REQUEST USE THIS

        $post_id = intval($post_id);

        if (empty($post_id)){
            return $this->response_error(CI_Core::RESPONSE_GENERIC_WRONG_PARAMS);
        }

        try
        {
            $post = new Post_model($post_id);
        } catch (EmeraldModelNoDataException $ex){
            return $this->response_error(CI_Core::RESPONSE_GENERIC_NO_DATA);
        }


        $posts =  Post_model::preparation($post, 'full_info');
        return $this->response_success(['post' => $posts]);
    }


    public function comment() { // or can be App::get_ci()->input->post('news_id') , but better for GET REQUEST USE THIS ( tests )

        if (!User_model::is_logged()){
            return $this->response_error(CI_Core::RESPONSE_GENERIC_NEED_AUTH);
        }

        $request = $this->jsonRequest();

        // загружаем библиотеки
        $this->load->library(['form_validation']);

        App::get_ci()->load->model('Comment_model');

        // сетим данные и правила валидации
        $this->form_validation->set_rules($this->Comment_model->create_rules);
        $this->form_validation->set_data($request);

        // проверяем соответсвуют ли данные нашим правилам
        if (!$this->form_validation->run()){
            return $this->response_error(CI_Core::RESPONSE_GENERIC_WRONG_PARAMS);
        }

        $parent_id = $request['parent_id'];
        $post_id = $request['post_id'];
        $message = $request['body'];

        try
        {
            $post = new Post_model($post_id);

            $comment =
                Comment_model::create([
                    'parent_id' => $parent_id,
                    'user_id' => User_model::get_session_id(),
                    'assign_id' => $post->get_id(),
                    'text' => $message,
                ])
            ;

            $parentComment = new Comment_model($parent_id);
            $parentComment->increaseCommentCount();

            return $this->response_success(['comments' => Comment_model::preparation([$comment], Comment_model::RESOURCE_INFO)]);

        } catch (EmeraldModelNoDataException $ex){
            return $this->response_error(CI_Core::RESPONSE_GENERIC_NO_DATA);
        }


//        $posts =  Post_model::preparation($post, 'full_info');

//        return $this->response_success(['post' => $posts]);
    }


    public function login()
    {
        // загружаем библиотеки
        $this->load->library(['form_validation']);

        $request = $this->jsonRequest();

        // сетим данные и правила валидации
        $this->form_validation->set_rules($this->Login_model->login_rules);
        $this->form_validation->set_data($request);

        // валидируем данные
        // и пытаемся найти пользователя с преденными логином и паролем
        if (
            $this->form_validation->run()
            &&
            (
                null !== ($user = User_model::find_by_login_and_password($request['login'], $request['password']))
            )
        ) {
            // в случае успеха стартуем сессию
            Login_model::start_session($user['id']);

            return $this->response_success(['user' => $user['id']]);
        }

        // в любых других случаях - ошибка
        return $this->response_error(CI_Core::RESPONSE_GENERIC_WRONG_PARAMS);
    }

    public function logout()
    {
        Login_model::logout();
        redirect(site_url('/'));
    }

    public function add_money(){
        // проверяем авторизован ли пользователь
        if (!User_model::is_logged()){
            return $this->response_error(CI_Core::RESPONSE_GENERIC_NEED_AUTH);
        }

        $user = User_model::getAuthUser();
        $request = $this->jsonRequest();

        $amount = intval($request['sum']);

        if ($amount < 1) {
            return $this->response_error(CI_Core::RESPONSE_GENERIC_WRONG_PARAMS);
        }

        $user->addMoney($amount);

        return $this->response_success(['amount' => $user->get_wallet_balance()]);
    }

    public function buy_boosterpack(){
        // проверяем авторизован ли пользователь
        if (!User_model::is_logged()){
            return $this->response_error(CI_Core::RESPONSE_GENERIC_NEED_AUTH);
        }

        // загружаем библиотеки
        $this->load->library(['form_validation']);
        $this->load->model('Boosterpack_model');

        $request = $this->jsonRequest();

        // сетим данные и правила валидации
        $this->form_validation->set_rules($this->Boosterpack_model->buy_boosterpack_rules);
        $this->form_validation->set_data($request);

        // валидируем данные
        // и пытаемся найти пользователя с преденными логином и паролем
        if (!$this->form_validation->run()) {
            return $this->response_error(CI_Core::RESPONSE_GENERIC_WRONG_PARAMS);
        }

        // получаем залогиненого пользователя
        $user = User_model::getAuthUser();

        $boosterpack = new Boosterpack_model($request['id']);

        $likesAmount = $boosterpack->createLikes();
        $user->addLikes($likesAmount);

        return $this->response_success(['amount' => $likesAmount]);
    }


    public function like() {
        // проверяем авторизован ли пользователь
        if (!User_model::is_logged()){
            return $this->response_error(CI_Core::RESPONSE_GENERIC_NEED_AUTH);
        }

        // получаем залогиненого пользователя
        $user = User_model::getAuthUser();

        App::get_ci()->load->model('Likes_model');

        // загружаем библиотеки
        $this->load->library(['form_validation']);

        $request = $this->jsonRequest();

        // сетим данные и правила валидации
        $this->form_validation->set_rules($this->Likes_model->add_like_rules);
        $this->form_validation->set_data($request);

        // провереям может ли пользователь лайкать
        // можно разбить каждую проверку на отдельный шаг и возвращать конкретную ошибку
        // упростим до успех неудача
        if ($this->form_validation->run() && $user->canLike()) {
            if ($this->Likes_model->likeEntry($user->get_id(), $request['entry_id'], $request['entry_type'])) {
                $likesAmount = $this->Likes_model->getEntryLikesAmount($request['entry_id'], $request['entry_type']);

                return $this->response_success(['likes' => $likesAmount]); // Колво лайков под постом \ комментарием чтобы обновить
            }
        }

        return $this->response_error(CI_Core::RESPONSE_GENERIC_WRONG_PARAMS);
    }
}
