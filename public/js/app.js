axios.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';

axios.interceptors.response.use(function (response) {
	let csrfCookie = document.cookie.match(/csrf_cookie_name=([0-9a-zA-Z]+)(;|")?/);

	if (Array.isArray(csrfCookie) && csrfCookie.length === 3 && csrfCookie[1].length > 0) {
		csValue = csrfCookie[1];
	}

	return response;
});

 //axios.defaults.headers.common[csName] = csValue;


Vue.component('comment-form', {
	template: `
	<div>
		<svg @click="showCommentModal" style="width: 1.2em; cursor: pointer;" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24">
			<path d="M17,7H7A1,1,0,0,0,7,9H17a1,1,0,0,0,0-2Zm0,4H7a1,1,0,0,0,0,2H17a1,1,0,0,0,0-2Zm2-9H5A3,3,0,0,0,2,5V15a3,3,0,0,0,3,3H16.59l3.7,3.71A1,1,0,0,0,21,22a.84.84,0,0,0,.38-.08A1,1,0,0,0,22,21V5A3,3,0,0,0,19,2Zm1,16.59-2.29-2.3A1,1,0,0,0,17,16H5a1,1,0,0,1-1-1V5A1,1,0,0,1,5,4H19a1,1,0,0,1,1,1Z" />
		</svg>
		<form class="form-inline" v-if="showForm">
			<div class="form-group">
				<input type="text" class="form-control" v-model="commentText">
			</div>
			
			<button type="submit" class="btn btn-primary" @click.prevent="addSubComment">Add comment</button>
		</form>
	</div>
	`,
	data: function () {
		return {
			commentText: '',
			showForm: false
		}
	},
	props: [
		'post_id',
		'parent_id',
	],
	methods: {
		showCommentModal() {
			//$('#commentModal').modal('show');
			console.log('sho comment modal');
			this.showForm = !this.showForm;
		},
		addSubComment() {
			let request = {
				"post_id": this.post_id,
				"parent_id": this.parent_id,
				"body": this.commentText
			};
			request[csName] = csValue;

			axios
				.post('/main_page/comment', {
					[csName]: csValue,
					"post_id": this.post_id,
					"parent_id": this.parent_id,
					"body": this.commentText
				})
				.then((response) => {
					if (response.data.status === 'success') {
						this.$emit('new_comment', response.data.comments[0]);
						this.commentText = '';
						this.showForm = false;
					}
				})
		}
	}
});

Vue.component('comment', {
	name: "comment",
	template: `
	<p class="card-text">
		
		
		{{comment.user.personaname + ' - '}}
		<small class="text-muted">{{comment.text}}</small>
		<likes v-bind:id="comment.id" v-bind:type="'comment'" v-bind:likes="comment.likes"></likes>
		<comment-form @new_comment="onNewComment" v-bind:post_id="post_id" v-bind:parent_id="comment.id"></comment-form>
		<comment
			v-for="com in comment.children"
			v-bind:comment="com"
			v-bind:post_id="post_id"
			v-bind:key="com.id"
			v-bind:parent_id="comment.id" />
		
	
		
	</p>
	`,
	props: [
		'comment',
		'post_id',
		'parent_id',
	],
	methods: {
		showCommentModal() {
			this.showForm = !this.showForm;
		},
		onNewComment(newComment) {
			this.comment.children.push(newComment);
		}
	}
});

Vue.component('likes', {
	data: function () {
		return {
            count: 0
        }
	},
	props: [
		'id',
		'type',
		'likes',
	],
    created() {
        this.count = this.likes;
    },
	template: `<div class="likes" @click="addLike">
                <div class="heart-wrap" v-if="!count">
                  <div class="heart">
                    <svg class="bi bi-heart" width="1em" height="1em" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                      <path fill-rule="evenodd" d="M8 2.748l-.717-.737C5.6.281 2.514.878 1.4 3.053c-.523 1.023-.641 2.5.314 4.385.92 1.815 2.834 3.989 6.286 6.357 3.452-2.368 5.365-4.542 6.286-6.357.955-1.886.838-3.362.314-4.385C13.486.878 10.4.28 8.717 2.01L8 2.748zM8 15C-7.333 4.868 3.279-3.04 7.824 1.143c.06.055.119.112.176.171a3.12 3.12 0 01.176-.17C12.72-3.042 23.333 4.867 8 15z" clip-rule="evenodd"/>
                    </svg>
                  </div>
                  <span>0</span>
                </div>
                <div class="heart-wrap" v-else>
                  <div class="heart">
                    <svg class="bi bi-heart-fill" width="1em" height="1em" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                      <path fill-rule="evenodd" d="M8 1.314C12.438-3.248 23.534 4.735 8 15-7.534 4.736 3.562-3.248 8 1.314z" clip-rule="evenodd"/>
                    </svg>
                  </div>
                  <span>{{ count }}</span>
                </div>
              </div>`,
	methods: {
		addLike: function () {
			axios
				.post('/main_page/like', {
					[csName]: csValue,
					"entry_id": this.id,
					"entry_type": this.type,
				})
				.then((response) => {
					if (response.data.status === 'success') {
						this.count = response.data.likes;
						this.$emit('likes_change', this.count);
					}
				})

		}
	}
});

var app = new Vue({
	el: '#app',
	data: {
		login: '',
		pass: '',
		post: false,
		invalidLogin: false,
		invalidPass: false,
		invalidSum: false,
		posts: [],
		addSum: 0,
		amount: 0,
		likes: 0,
		commentText: '',
		packs: [
			{
				id: 1,
				price: 5
			},
			{
				id: 2,
				price: 20
			},
			{
				id: 3,
				price: 50
			},
		],
	},
	computed: {
		test: function () {
			var data = [];
			return data;
		}
	},
	created(){
		var self = this
		axios
			.get('/main_page/get_all_posts')
			.then(function (response) {
				self.posts = response.data.posts;
			})
	},
	methods: {
		logout: function () {
			console.log ('logout');
		},
		logIn: function () {
			var self= this;
			if(self.login === ''){
				self.invalidLogin = true
			}
			else if(self.pass === ''){
				self.invalidLogin = false
				self.invalidPass = true
			}
			else{
				self.invalidLogin = false
				self.invalidPass = false
				axios.post('/main_page/login', {
					[csName] : csValue,
					login: self.login,
					password: self.pass
				})
					.then(function (response) {
						document.location.reload(true);
						// setTimeout(function () {
						// 	$('#loginModal').modal('hide');
						// }, 500);
					})
			}
		},
		fiilIn: function () {
			var self= this;
			if(self.addSum === 0){
				self.invalidSum = true
			}
			else{
				self.invalidSum = false
				axios.post('/main_page/add_money', {
					[csName] : csValue,
					sum: self.addSum,
				})
					.then(function (response) {
						setTimeout(function () {
							$('#addModal').modal('hide');
						}, 500);
					})
			}
		},
		openPost: function (id) {
			var self= this;
			axios
				.get('/main_page/get_post/' + id)
				.then(function (response) {
					self.post = response.data.post;
					if(self.post){
						setTimeout(function () {
							$('#postModal').modal('show');
						}, 500);
					}
				})
		},
		addComment: function () {
			var self= this;

			axios
				.post('/main_page/comment', {
					[csName] : csValue,
					"post_id": self.post.id,
					"body": self.commentText
				})
				.then(function (response) {
					if (response.data.status === 'success') {
						self.post.coments.push(response.data.comments[0]);
						self.commentText = '';
					}
				})

		},
		buyPack: function (id) {
			var self= this;
			axios.post('/main_page/buy_boosterpack', {
				[csName] : csValue,
				id: id,
			})
				.then(function (response) {
					self.amount = response.data.amount
					if(self.amount !== 0){
						setTimeout(function () {
							$('#amountModal').modal('show');
						}, 500);
					}
				})
		},
		onLikesChange: function (likesCount) {
			console.log('new likes count is ' + likesCount);
		}
	}
});
